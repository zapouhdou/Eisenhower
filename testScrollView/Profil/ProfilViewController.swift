//
//  ProfilViewController.swift
//  testScrollView
//
//  Created by Zebre on 11/03/2018.
//  Copyright © 2018 Zebre. All rights reserved.
//

import Foundation
import UIKit

class ProfilViewController: UIViewController {
    
    var profilImageView = ProfilImageView()
    var nameView = LabelView()
    var emailView = LabelView()
    var descriptionView = DescriptionView()
    var idUser = 0
    func initData(user: User)
    {
        idUser = user.id!
        nameView.labelView?.text = user.username
        emailView.labelView?.text = user.email
        descriptionView.descriptionView?.text = user.description
        profilImageView.imageView?.image = UIImage(named: user.picture!)
    }
    let saveChangeButton: UIButton = {
        let button = UIButton()
        button.setTitle("Save changes", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.backgroundColor = UIColor.rgb(242, green: 186, blue: 71, alpha: 1.0)
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.clear.cgColor
        button.addTarget(self, action: #selector(saveChange), for: .touchUpInside)
        button.titleLabel?.font =  UIFont.systemFont(ofSize: 24, weight: .light)
        return button
    }()
    
    let logoutButton: UIButton = {
        let button = UIButton()
        button.setTitle("logout", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.backgroundColor = UIColor.rgb(223, green: 225, blue: 66, alpha: 1.0)
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.clear.cgColor
        button.addTarget(self, action: #selector(logout), for: .touchUpInside)
        button.titleLabel?.font =  UIFont.systemFont(ofSize: 18, weight: .light)
        return button
    }()
    
    let renewPasswordButton: UIButton = {
        let button = UIButton()
        button.setTitle("Renew password", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.backgroundColor = UIColor.rgb(126, green: 209, blue: 70, alpha: 1.0)
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.clear.cgColor
        button.addTarget(self, action: #selector(renewPassword), for: .touchUpInside)
        button.titleLabel?.font =  UIFont.systemFont(ofSize: 14, weight: .light)
        return button
    }()
    
    @objc func logout()
    {
        
    }
    @objc func saveChange()
    {
        
    }
    
    @objc func renewPassword()
    {
    
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        self.navigationItem.title = "Task Detail (Edit)"
        
        
        
        self.view.backgroundColor = UIColor.rgb(128, green: 202, blue: 220, alpha: 1)
        
       
        
        profilImageView.backgroundColor = UIColor.clear
        self.view.addSubview(profilImageView)
        self.view.addSubview(nameView)
        self.view.addSubview(descriptionView)
        self.view.addSubview(emailView)
        self.view.addSubview(logoutButton)
        self.view.addSubview(renewPasswordButton)
        self.view.addSubview(saveChangeButton)
     
        
        self.view.addConstraintsWithFormat("H:|-20-[v0]-20-|", views: profilImageView)
        self.view.addConstraintsWithFormat("H:|-20-[v0]-20-|", views: nameView)
        self.view.addConstraintsWithFormat("H:|-20-[v0]-20-|", views: descriptionView)
          self.view.addConstraintsWithFormat("H:|-[v0(200)]-|", views: saveChangeButton)
         self.view.addConstraintsWithFormat("H:|-[v0(160)]-|", views: logoutButton)
        self.view.addConstraintsWithFormat("H:|-[v0(130)]-20-|", views: renewPasswordButton)
        self.view.addConstraintsWithFormat("H:|-20-[v0]-20-|", views: emailView)
        //   self.view.addConstraintsWithFormat("H:|-20-[v0]-20-|", views: friendCollectionView)
        self.view.addConstraintsWithFormat("V:|-90-[v0(80)]-10-[v1(50)]-10-[v2(200)]-10-[v3(50)]-5-[v4(30)]-5-[v5(55)]-5-[v6(40)]", views: profilImageView, nameView, descriptionView, emailView, renewPasswordButton, saveChangeButton, logoutButton)
    }
    
    func hideForFriendProfile()
    {
        renewPasswordButton.isHidden = true
        saveChangeButton.isHidden = true
        logoutButton.isHidden = true
        profilImageView.titleLabel?.isUserInteractionEnabled = false
        nameView.labelView?.isUserInteractionEnabled = false
        descriptionView.descriptionView?.isUserInteractionEnabled = false
        emailView.labelView?.isUserInteractionEnabled = false
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
