//
//  TaskDetailViewController.swift
//  testScrollView
//
//  Created by Zebre on 10/03/2018.
//  Copyright © 2018 Zebre. All rights reserved.
//

import Foundation
import UIKit

class TaskDetailViewController: UIViewController {

    let saveChangeButton: UIButton = {
        let button = UIButton()
        button.setTitle("Save changes", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.backgroundColor = UIColor.rgb(242, green: 186, blue: 71, alpha: 1.0)
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.clear.cgColor
        button.addTarget(self, action: #selector(saveChange), for: .touchUpInside)
        button.titleLabel?.font =  UIFont.systemFont(ofSize: 24, weight: .light)
        return button
    }()
    
    let deleteTaskButton: UIButton = {
        let button = UIButton()
        button.setTitle("Delete Task", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.backgroundColor = UIColor.rgb(223, green: 225, blue: 66, alpha: 1.0)
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.clear.cgColor
        button.addTarget(self, action: #selector(deleteTask), for: .touchUpInside)
        button.titleLabel?.font =  UIFont.systemFont(ofSize: 18, weight: .light)
        return button
    }()
    
    @objc func saveChange()
    {
        
    }
    @objc func deleteTask()
    {
        
    }
    
    var titleView = TitleView()
    var friendView = FriendView()
    var descriptionView = DescriptionView()
    var dateView = LabelView()
    
    func initData(post: Task?){
        print("YOYOYOYOOY")
            if let toto = post?.title
            {
                titleView.changeText(text: toto)
                titleView.titleLabel?.text = toto
            }
            if let description = post?.description
            {
                descriptionView.descriptionView?.text = description
            }
            
            if let date = post?.date
            {
               dateView.labelView?.text = date
            }
            if let users = post?.users
            {
            friendView.post = users
                
            }
    }
    
  
  @objc func UserWantSeeProfil(_ notification: NSNotification) {
        
        if let id = notification.userInfo?["id"] as? Int {
            // do something with your image
            if let listFriend = Network.sharedInstance.listUser
            {
                print("VEGETA \(listFriend.first(where: { $0.id == id })?.toJSONString())")
                
                if let user = listFriend.first(where: { $0.id == id }){
                    let nextVC = ProfilViewController()
                    nextVC.initData(user: user)
                    nextVC.hideForFriendProfile()
                    self.navigationController?.pushViewController(nextVC, animated: true)
                }
            }
           
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        self.navigationItem.title = "Task Detail (Edit)"
   
        NotificationCenter.default.addObserver(self, selector: #selector(self.UserWantSeeProfil(_:)), name: NSNotification.Name(rawValue: "profilKey"), object: nil)
        
      
      
        
        self.view.backgroundColor = UIColor.rgb(128, green: 202, blue: 220, alpha: 1)
      friendView.isUserInteractionEnabled = true
        
        
        self.view.addSubview(titleView)
        self.view.addSubview(friendView)
        self.view.addSubview(descriptionView)
        self.view.addSubview(dateView)
        self.view.addSubview(saveChangeButton)
        self.view.addSubview(deleteTaskButton)
        
     
        //self.view.addSubview(friendCollectionView!)
        
        
        self.view.addConstraintsWithFormat("H:|-20-[v0]-20-|", views: titleView)
        self.view.addConstraintsWithFormat("H:|-20-[v0]-20-|", views: friendView)
        self.view.addConstraintsWithFormat("H:|-20-[v0]-20-|", views: descriptionView)
        self.view.addConstraintsWithFormat("H:|-20-[v0]-20-|", views: dateView)
          self.view.addConstraintsWithFormat("H:|-100-[v0]-100-|", views: saveChangeButton)
          self.view.addConstraintsWithFormat("H:|-120-[v0]-120-|", views: deleteTaskButton)
      //   self.view.addConstraintsWithFormat("H:|-20-[v0]-20-|", views: friendCollectionView!)
        self.view.addConstraintsWithFormat("V:|-90-[v0(100)]-10-[v1(100)]-10-[v2(200)]-10-[v3(50)]-5-[v4(50)]-5-[v5(40)]", views: titleView, friendView, descriptionView, dateView, saveChangeButton, deleteTaskButton)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
