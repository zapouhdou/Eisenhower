//
//  TitleView.swift
//  testScrollView
//
//  Created by Zebre on 11/03/2018.
//  Copyright © 2018 Zebre. All rights reserved.
//

import Foundation
import UIKit

class TitleView : UIView
{
    override init(frame: CGRect) {
        super.init(frame: frame)
        addBehavior()
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
          addBehavior()
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    func changeText(text : String)
    {
        print("Change text")
      keyText = text
    }
    
    func addBehavior() {
        
        
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = true;
        self.backgroundColor = UIColor.rgb(114, green: 181, blue: 197, alpha: 0.9)
        titleLabel = UILabel()
        titleLabel?.numberOfLines = 3
       
        titleLabel?.textColor = UIColor.white
          titleLabel?.text = keyText
        
        titleLabel?.font = UIFont.systemFont(ofSize: 21, weight: .medium)
        button = UIImageView(image: UIImage(named: "important"))
        
        self.addSubview(titleLabel!)
        self.addSubview(button!)
        self.addConstraintsWithFormat("H:|-5-[v0]-5-[v1(42)]-5-|", views: titleLabel!, button!)
        self.addConstraintsWithFormat("V:|-[v0]-|", views: titleLabel!)
        self.addConstraintsWithFormat("V:|-[v0(42)]-|", views: button!)
    }
    var titleLabel : UILabel?
    var button : UIImageView?
    var keyText: String?
}
