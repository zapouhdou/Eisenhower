//
//  ProfileImageView.swift
//  testScrollView
//
//  Created by Zebre on 11/03/2018.
//  Copyright © 2018 Zebre. All rights reserved.
//

import Foundation
import UIKit

class ProfilImageView : UIView
{
    override init(frame: CGRect) {
        super.init(frame: frame)
        addBehavior()
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    func addBehavior() {
        
        
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = true;
        self.backgroundColor = UIColor.rgb(114, green: 181, blue: 197, alpha: 0.9)

        let blankView = UIView()
        let blankView1 = UIView()
        imageView = UIImageView(image: UIImage(named: "important"))
        imageView!.layer.cornerRadius = self.frame.size.width / 2
        imageView!.clipsToBounds = true
        self.addSubview(imageView!)
        self.addSubview(blankView)
        self.addSubview(blankView1)
        
        self.addConstraintsWithFormat("H:|[v0(v2)]-[v1(120)]-[v2(v0)]|", views: blankView, imageView!, blankView1)
        self.addConstraintsWithFormat("V:|[v0(120)]|", views: blankView)
        self.addConstraintsWithFormat("V:|[v0(120)]|", views: blankView1)
        self.addConstraintsWithFormat("V:|[v0(120)]|", views: imageView!)
    }
    var titleLabel : UILabel?
    var imageView : UIImageView?
}
