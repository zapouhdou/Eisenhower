//
//  DescriptionView.swift
//  testScrollView
//
//  Created by Zebre on 11/03/2018.
//  Copyright © 2018 Zebre. All rights reserved.
//

import Foundation
import UIKit

class DescriptionView : UIView
{
    override init(frame: CGRect) {
        super.init(frame: frame)
        addBehavior()
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    func addBehavior() {
        
        
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = true;
        self.backgroundColor = UIColor.clear
        descriptionView = UITextView()
        descriptionView?.text = "Label in \n 3 \n line"
        descriptionView?.textColor = UIColor.white
        descriptionView?.backgroundColor = UIColor.rgb(114, green: 181, blue: 197, alpha: 0.9)
        
        descriptionView?.font = UIFont.systemFont(ofSize: 12, weight: .medium)
      
        self.addSubview(descriptionView!)
       
        self.addConstraintsWithFormat("H:|-5-[v0]-5-|", views: descriptionView!)
        self.addConstraintsWithFormat("V:|-5-[v0]-5-|", views: descriptionView!)
      
    }
    var descriptionView : UITextView?
   
}
