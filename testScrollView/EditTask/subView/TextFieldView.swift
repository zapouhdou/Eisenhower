//
//  textFieldView.swift
//  testScrollView
//
//  Created by Zebre on 25/03/2018.
//  Copyright © 2018 Zebre. All rights reserved.
//

import Foundation
import UIKit
class TextFieldView : UIView
{
    override init(frame: CGRect) {
        super.init(frame: frame)
        addBehavior()
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    func addBehavior() {
        
        
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = true;
        self.backgroundColor = UIColor.rgb(114, green: 181, blue: 197, alpha: 0.9)
        labelView = UITextField()
        labelView?.text = "********"
        labelView?.textColor = UIColor.white
        labelView?.isSecureTextEntry = true
        
        labelView?.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        
        self.addSubview(labelView!)
        
        self.addConstraintsWithFormat("H:|-5-[v0]-5-|", views: labelView!)
        self.addConstraintsWithFormat("V:|-5-[v0]-5-|", views: labelView!)
        
    }
    var labelView : UITextField?
    
}
