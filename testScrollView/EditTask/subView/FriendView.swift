//
//  FriendView.swift
//  testScrollView
//
//  Created by Zebre on 11/03/2018.
//  Copyright © 2018 Zebre. All rights reserved.
//

import Foundation
import UIKit

class FriendView : UIView, UICollectionViewDataSource, UICollectionViewDelegate
{
    var nbrFriend : Int?
    
    var listFriend: [UserLess]?

    var post: [UserLess]? {
        didSet {
           
           
            
            if let list = post
            {
                listFriend = list
                if let nbr = post?.count
                {
                    nbrFriend = nbr
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let nbr = nbrFriend
        {
            return nbr
        }
        return 0
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let feedCell = collectionView.dequeueReusableCell(withReuseIdentifier: "friendCell", for: indexPath) as! FriendCell
        
        
        let friend = listFriend![indexPath.row]
 
        feedCell.profileImageView.image = UIImage(named: friend.picture!)
        feedCell.setId(id: friend.id!)
        feedCell.backgroundColor = UIColor.rgb(114, green: 181, blue: 197, alpha: 0.9)
        
        return feedCell
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addBehavior()
    }
    
    convenience init() {
        self.init(frame: CGRect.zero)
    }
    
    required init(coder aDecoder: NSCoder) {
        fatalError("This class does not support NSCoding")
    }
    
    func addBehavior() {
        
        
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = true;
        self.backgroundColor = UIColor.rgb(114, green: 181, blue: 197, alpha: 0.9)
        
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 60, height: 60)
        layout.scrollDirection = .horizontal
        
        friendCollectionView = UICollectionView(frame: CGRect.zero, collectionViewLayout: layout)
        friendCollectionView?.dataSource = self
        friendCollectionView?.delegate = self
        friendCollectionView?.isUserInteractionEnabled = true
        friendCollectionView?.backgroundColor = UIColor.rgb(114, green: 181, blue: 197, alpha: 0.9)
        friendCollectionView?.register(FriendCell.self, forCellWithReuseIdentifier: "friendCell")
        
        
        
        addFriend = UIImageView(image: UIImage(named: "important"))
    
        addFriend!.contentMode = UIViewContentMode.scaleAspectFit
        self.addSubview(friendCollectionView!)
        self.addSubview(addFriend!)
        self.addConstraintsWithFormat("H:|-5-[v0]-5-[v1(60)]-5-|", views: friendCollectionView!, addFriend!)
        self.addConstraintsWithFormat("V:|-20-[v0(60)]-20-|", views: friendCollectionView!)
        self.addConstraintsWithFormat("V:|-20-[v0(60)]-20-|", views: addFriend!)
    }

    var friendCollectionView : UICollectionView?
    var addFriend : UIImageView?
}
