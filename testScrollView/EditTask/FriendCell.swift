//
//  FriendCell.swift
//  testScrollView
//
//  Created by Zebre on 11/03/2018.
//  Copyright © 2018 Zebre. All rights reserved.
//

import Foundation
import UIKit
class FriendCell: UICollectionViewCell {
    let nameEvent: UITextField = {
        let label = UITextField()
        label.textAlignment = .center
        label.text = ""
        return label
    }()
    let profileImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = UIColor.clear
        return imageView
    }()
   var idFriend: Int = 0
    
    func setId(id: Int)
    {
        idFriend = id
    }
  
  /*  var post: UserResponse? {
        didSet {
            
            if let id = post?.id
            {
                idFriend = id
                print(id)
            }
            if let picture = post?.picture {
                
                let session = URLSession.shared
                let req = URLRequest(url: URL(string: picture)!)
                var task = session.dataTask(with: req){ (data,response,error) in
                    
                    if error != nil {
                        print(error)
                        return
                    }
                    
                    if let resp = response?.url?.absoluteString
                    {
                        if (resp.contains("png") || resp.contains("jpeg") || resp.contains("jpg"))
                        {
                            
                            let image = UIImage(data: data!)
                            DispatchQueue.main.async {
                                
                                self.profileImageView.image = image
                                self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width / 2;
                            }
                            return
                        }
                        else
                        {
                            DispatchQueue.main.async {
                                self.profileImageView.image = UIImage(named: "defaultUserPicture")
                                
                            }
                        }
                    }
                    }.resume()
            }
            
        }
    }
    */
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @objc func touchCell()
    {
        let dict:[String: Int] = ["id": idFriend]
        NotificationCenter.default.post(name: Notification.Name(rawValue: "profilKey"), object: self, userInfo: dict)
    
            
    }
            
    
        
        /*    print("User ID = \(self.idFriend)")
        Network.sharedInstance.getMyFriend()
        Network.sharedInstance.getUserById(id: self.idFriend, completion: {
            print("Completion Works")
            
            
            if let toto = Network.sharedInstance.friendSheet {
                let nextVC = ProfilFriendViewController()
                
                if let tito = toto.friendUser?.id
                {
                    nextVC.initProfile(user: toto)
                    self.navController.pushViewController(nextVC, animated: false)
                }
                
                
            }
        })
        
        
       */
    
    func setupViews(){
        self.addSubview(nameEvent)
        self.addSubview(profileImageView)
        let tap = UITapGestureRecognizer(target: self, action: #selector(FriendCell.touchCell))
        self.backgroundColor = UIColor.clear
        self.addGestureRecognizer(tap)
        self.addConstraintsWithFormat("H:|[v0]|", views: profileImageView)
        self.addConstraintsWithFormat("V:|[v0]|", views: profileImageView)
        nameEvent.textAlignment = .center
    }
    
}
