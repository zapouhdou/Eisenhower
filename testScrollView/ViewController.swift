//
//  ViewController.swift
//  testScrollView
//
//  Created by Zebre on 23/01/2018.
//  Copyright © 2018 Zebre. All rights reserved.
//

import UIKit
extension UIView {
func addConstraintsWithFormat(_ format: String, views: UIView...) {
    var viewsDictionary = [String: UIView]()
    for (index, view) in views.enumerated() {
        let key = "v\(index)"
        viewsDictionary[key] = view
        view.translatesAutoresizingMaskIntoConstraints = false
    }
    
    addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
}
}
class ViewController: UIViewController {

    var scrollView = UIScrollView()
    var containerView = UIView()
   
    
    var profileImageView: UIView!
    var imageView: UIImageView!
    var userNameField: UILabel!
    
    var testView = UIView()
    
    
    func initProfilImageView()
    {
        imageView = UIImageView()
        userNameField = UILabel()
        userNameField.text = "Pierre"
        userNameField.textColor = UIColor.black
        userNameField.textAlignment = .center
        let blankView1 = UIView()
        let blankView = UIView()
        imageView.backgroundColor = UIColor.red
        profileImageView = UIView()
        profileImageView.addSubview(blankView)
        profileImageView.addSubview(imageView)
        profileImageView.addSubview(blankView1)
        profileImageView.addSubview(userNameField)
        
        profileImageView.addConstraintsWithFormat("H:|[v0(v1)][v1(v2)][v2(v0)]|", views: blankView, imageView, blankView1)
          profileImageView.addConstraintsWithFormat("H:|[v0(v1)][v1(v2)][v2(v0)]|", views: blankView, userNameField, blankView1)
        profileImageView.addConstraintsWithFormat("V:|[v0]|", views: blankView)
        profileImageView.addConstraintsWithFormat("V:|[v0]-[v1]|", views: imageView, userNameField)
        profileImageView.addConstraintsWithFormat("V:|[v0]|", views: blankView1)
        
        self.containerView.addSubview(profileImageView)
        self.containerView.addConstraintsWithFormat("H:|[v0]|", views: profileImageView)
        self.containerView.addConstraintsWithFormat("V:|[v0(200)]", views: profileImageView)
    }
    func initView()
    {
        let viewWidth = "\(self.view.frame.size.width)"
        let viewHeight = "\(self.view.frame.size.height)"
        
        scrollView = UIScrollView()
        containerView = UIView()
         self.view.addSubview(scrollView)
         self.scrollView.addSubview(containerView)
        self.view.addConstraintsWithFormat("H:|[v0]|", views: scrollView)
        self.view.addConstraintsWithFormat("V:|-44-[v0]|", views: scrollView)
        self.scrollView.addConstraintsWithFormat("H:|[v0(\(viewWidth))]|", views: containerView)
        self.scrollView.addConstraintsWithFormat("V:|-44-[v0(\(viewHeight))]|", views: containerView)
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.white
        
    initView()
    initProfilImageView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

