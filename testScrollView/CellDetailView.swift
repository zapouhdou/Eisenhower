//
//  CellDetailView.swift
//  testScrollView
//
//  Created by Zebre on 27/02/2018.
//  Copyright © 2018 Zebre. All rights reserved.
//

import Foundation
import UIKit


let identifierCellAdd = "addItem"
let identifierText = "textItem"


class CellDetailController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UICollectionViewDelegate {

    var collectionView : UICollectionView?
    var taskUser : [Task]?
    
     func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
     
        
        if (indexPath.row == 0)
        {
         // add Task
        }
        else
        {
        let toto = TaskDetailViewController()
      print("HERE MTF")
        print(taskUser![indexPath.row - 1].toJSON())
        toto.initData(post: taskUser![indexPath.row - 1])
              self.navigationController?.pushViewController(toto, animated: true)
        }
        
  
        
     }
     
     
     //    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForFooterInSection section: Int) -> CGSize {
     //
     //
     //        return CGSize(width:self.view.frame.width, height: 100)
     //
     //    }
     //
    
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let padding: CGFloat =  100
//        let collectionViewSize = collectionView.frame.size.width - padding
//
//        return CGSize(width: collectionViewSize/2 , height: 60)
//    }
     func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       
        let feedCell = collectionView.dequeueReusableCell(withReuseIdentifier: identifierCellAdd, for: indexPath) as! HomeCell
    
        
        if (indexPath.row == 0)
        {
            feedCell.setupViews(type: .addCell)
        }
        else
        {
           if let level = taskUser![indexPath.row - 1].level
           {
            switch (level)
            {
            case 1:
                 feedCell.setupViews(type: .greeLightCell)
            case 2:
                 feedCell.setupViews(type: .blueCell)
            case 3:
                 feedCell.setupViews(type: .greenCell)
            case 4:
                 feedCell.setupViews(type: .redCell)
            default:
                 feedCell.setupViews(type: .redCell)
            }
            }
            
                feedCell.post = taskUser![indexPath.row - 1]
            
        }
        
       
        return feedCell
    }
    
 
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        // +1 pour faire appparaitre le bouton add
        if let count = taskUser?.count
        {
            return (count + 1)
        }
        return 1
        
    }
    
    @objc func showProfilViewController()
    {
        let nextVC = ProfilViewController()
        
        if let currentUser = Network.sharedInstance.currentUser
        {
            nextVC.initData(user: currentUser)
            self.navigationController?.pushViewController(nextVC, animated: true)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if let currentuser =  Network.sharedInstance.currentUser{
            if let id = currentuser.id
            {
            switch (id){
            case 0:
               taskUser = Network.sharedInstance.listTaskPierre
            case 1:
                taskUser = Network.sharedInstance.listTaskLaurent
            case 2:
               taskUser = Network.sharedInstance.listTaskAbdelhamid
            case 3:
            taskUser = Network.sharedInstance.listTaskNicolas
            default:
              taskUser = Network.sharedInstance.listTaskPierre
            }
            }
        }
        
          self.navigationItem.title = "Home"
        let profileButton = UIBarButtonItem(image: UIImage(named: "profile"), style: .done, target: self, action: #selector(showProfilViewController))
 
        
       self.navigationItem.rightBarButtonItem = profileButton
        let toto = UICollectionViewFlowLayout()
       toto.sectionInset = UIEdgeInsets(top: 30, left: 20, bottom: 100, right: 20)
        
        let widthItem = self.view.frame.width/2 - 30
        toto.itemSize = CGSize(width: widthItem, height: 80)
    
        self.view.backgroundColor = UIColor.white
        self.collectionView = UICollectionView(frame: self.view.frame, collectionViewLayout: toto)
        self.view.addSubview(self.collectionView!)
        self.collectionView?.delegate   = self
        self.collectionView?.dataSource = self
        self.collectionView?.backgroundColor = UIColor.rgb(128, green: 202, blue: 220, alpha: 1)
        self.collectionView?.alwaysBounceVertical = true
        self.collectionView?.register(HomeCell.self, forCellWithReuseIdentifier: identifierCellAdd)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
