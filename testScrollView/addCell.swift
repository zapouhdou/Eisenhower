//
//  addCell.swift
//  testScrollView
//
//  Created by Zebre on 27/02/2018.
//  Copyright © 2018 Zebre. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    static func rgb(_ red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
  
    }
    
}

enum typeCase {
    case redCell
    case greenCell
    case blueCell
    case greeLightCell
    case addCell
    case normalCell
}

class HomeCell: UICollectionViewCell {
    
    
    var titleLabel : UILabel?
    var dateLabel : UILabel?
    var imageAlert : UIImageView?
    var imageSnooze : UIImageView?
    
    var post: Task? {
        didSet {
            if let toto = post?.title
            {
                titleLabel?.text = toto
            }
            if let date = post?.date
            {
                dateLabel?.text = date
            }
        }
    }
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupViews(type: typeCase.normalCell)
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    func eraseView()
    {
        for subView in self.subviews
        {
            subView.removeFromSuperview()
        }
    }
   
    func setupAddButton()
    {
        let addButton = UIImageView(image: UIImage(named: "AddTask"))
        
        
        self.addSubview(addButton)
  
        addButton.frame.size.width = 40
        addButton.frame.size.height = 40
        self.addConstraintsWithFormat("H:|-[v0]-|", views: addButton)
        self.addConstraintsWithFormat("V:|-[v0]-|", views: addButton)
        
    }
    
    func setUpLayout()
    {
        titleLabel = UILabel()
        titleLabel?.text = "Title alert title alert"
        titleLabel?.numberOfLines = 2
        titleLabel?.textColor = UIColor.white
        titleLabel?.font = UIFont.systemFont(ofSize: 12, weight: .regular)
        dateLabel = UILabel()
        dateLabel?.text = "12 Jan 2017"
        dateLabel?.font = UIFont.systemFont(ofSize: 12, weight: .light)
        dateLabel?.textColor = UIColor.white
        imageAlert = UIImageView(image: UIImage(named: "important"))
        imageSnooze = UIImageView(image: UIImage(named: "UrgentIcon"))
        let blankView = UIView()
        self.addSubview(titleLabel!)
        self.addSubview(dateLabel!)
        self.addSubview(imageSnooze!)
        self.addSubview(imageAlert!)
        self.addSubview(blankView)
        
        
        self.addConstraintsWithFormat("H:|-8-[v0]-5-[v1(28)]-8-|", views: titleLabel!, imageAlert!)
        self.addConstraintsWithFormat("H:|-8-[v0]-5-[v1(28)]-8-|", views: dateLabel!, imageSnooze!)
        self.addConstraintsWithFormat("H:|[v0]|", views: blankView)
        self.addConstraintsWithFormat("V:|-8-[v0(30)]-[v1]-[v2(30)]-8-|", views: titleLabel!, blankView, dateLabel!)
        self.addConstraintsWithFormat("V:|-8-[v0(28)]-[v1]-[v2(28)]-8-|", views: imageAlert!, blankView, imageSnooze!)
    
    }
   
    func setupViews(type : typeCase) {

        self.layer.cornerRadius = 5.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = UIColor.clear.cgColor
        self.layer.masksToBounds = true
        
        switch type
        {
        case .greenCell:
            self.backgroundColor = UIColor.rgb(126, green: 211, blue: 33, alpha: 0.8)
            setUpLayout()
            self.imageAlert?.alpha = 0.2
        case .addCell:
              self.backgroundColor = UIColor.rgb(128, green: 202, blue: 220, alpha: 1)
            setupAddButton()
        case .redCell:
              self.backgroundColor = UIColor.rgb(255, green: 58, blue: 7, alpha: 0.8)
            setUpLayout()
        case .blueCell:
              self.backgroundColor = UIColor.rgb(13, green: 160, blue: 178, alpha: 0.8)
              setUpLayout()
              self.imageSnooze?.alpha = 0.2
        case .greeLightCell:
              self.backgroundColor = UIColor.rgb(248, green: 232, blue: 28, alpha: 0.4)
              setUpLayout()
              self.imageAlert?.alpha = 0.2
              self.imageSnooze?.alpha = 0.2
        case .normalCell:
            print("Normal cell")
        }
    }
    
    
    
    
    
}
