//
//  LoginViewController.swift
//  testScrollView
//
//  Created by Zebre on 11/03/2018.
//  Copyright © 2018 Zebre. All rights reserved.
//

import Foundation
import UIKit
import FacebookLogin
import FacebookCore
import GoogleSignIn

class LoginViewController: UIViewController, GIDSignInDelegate, GIDSignInUIDelegate, LoginButtonDelegate {
    func loginButtonDidCompleteLogin(_ loginButton: LoginButton, result: LoginResult) {
        print("HELLO \(result)")
        switch result {
        case .failed(let error):
            print(error)
        case .cancelled:
            print("Cancelled")
        case .success(let grantedPermissions, let declinedPermissions, let accessToken):
            apikey = accessToken.authenticationToken
            let nextVC = CellDetailController()
            self.navigationController?.pushViewController(nextVC, animated: false)
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: LoginButton) {
        
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if error != nil{
            print(error ?? "google error")
            return
        }
          Network.sharedInstance.initData()
        
        let nextVC = CellDetailController()
        self.navigationController?.pushViewController(nextVC, animated: false)
        apikey = user.authentication.accessToken
        
       /* Network.sharedInstance.logIn(type: .googleLogin, token: user.authentication.accessToken, password: "", completion: {(success) in
           
            if (success)
            {
                print("Congratz, you are now connected with Google on our Api.")
            }
        })*/
       
    }
    
    var apikey : String?
    var emailView : LabelView?
    var passwordView : TextFieldView?
    var facebookButton : LoginButton?
    var googleButton: GIDSignInButton?
    
    let signInButton: UIButton = {
        let button = UIButton()
        button.setTitle("Sign In", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.backgroundColor = UIColor.rgb(223, green: 225, blue: 66, alpha: 1.0)
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.clear.cgColor
        button.addTarget(self, action: #selector(logout), for: .touchUpInside)
        button.titleLabel?.font =  UIFont.systemFont(ofSize: 18, weight: .light)
        return button
    }()
    
    
    let loginButton: UIButton = {
        let button = UIButton()
        button.setTitle("login", for: .normal)
        button.setTitleColor(UIColor.blue, for: .normal)
        button.backgroundColor = UIColor.rgb(223, green: 225, blue: 66, alpha: 1.0)
        button.layer.cornerRadius = 5
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.clear.cgColor
        button.addTarget(self, action: #selector(logout), for: .touchUpInside)
        button.titleLabel?.font =  UIFont.systemFont(ofSize: 18, weight: .light)
        return button
    }()
    
  
    
    @objc func logout()
    {
        
    }
    @objc func saveChange()
    {
        
    }
    
    @objc func renewPassword()
    {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.white
        self.navigationItem.title = "Task Detail (Edit)"
        

        if let accessToken = AccessToken.current {
            print("YOLO")
             apikey = accessToken.authenticationToken
            let nextVC = CellDetailController()
            self.navigationController?.pushViewController(nextVC, animated: false)
           
            
           
            // User is logged in, use 'accessToken' here.
        }
        self.view.backgroundColor = UIColor.rgb(128, green: 202, blue: 220, alpha: 1)
        
        passwordView = TextFieldView()
        passwordView?.isUserInteractionEnabled = true
        googleButton = GIDSignInButton()
        //adding the delegates
        GIDSignIn.sharedInstance().uiDelegate = self as! GIDSignInUIDelegate
        GIDSignIn.sharedInstance().delegate = self
        //getting the signin button and adding it to view
        let googleSignInButton = GIDSignInButton()
        googleSignInButton.center = view.center
        self.view.addSubview(googleSignInButton)
        GIDSignIn.sharedInstance().uiDelegate = self
        emailView = LabelView()
        emailView?.isUserInteractionEnabled = true
        
        
        
        facebookButton = LoginButton(readPermissions: [.publicProfile])
        
        facebookButton?.target(forAction: #selector(loginButtonClicked), withSender: self)
        self.view.addSubview(emailView!)
        self.view.addSubview(passwordView!)
        self.view.addSubview(loginButton)
        self.view.addSubview(googleSignInButton)
        self.view.addSubview(facebookButton!)
        self.view.addSubview(signInButton)
        emailView?.labelView?.text = "email"
        passwordView?.labelView?.text = "******"
      
        self.view.addConstraintsWithFormat("H:|-20-[v0]-20-|", views: emailView!)
        self.view.addConstraintsWithFormat("H:|-20-[v0]-20-|", views: passwordView!)
        self.view.addConstraintsWithFormat("H:|-20-[v0]-20-|", views: loginButton)
       
        self.view.addConstraintsWithFormat("H:|-[v0(150)]-20-[v1(150)]-|", views: googleSignInButton, facebookButton!)
      
        self.view.addConstraintsWithFormat("H:|-20-[v0]-20-|", views: signInButton)
        self.view.addConstraintsWithFormat("V:|-80-[v0(55)]-10-[v1(55)]-10-[v2(55)]-50-[v3(55)]-5-[v4(40)]", views: emailView!, passwordView!, loginButton, googleSignInButton, signInButton)
        self.view.addConstraintsWithFormat("V:|-80-[v0(55)]-10-[v1(55)]-10-[v2(55)]-50-[v3(55)]-5-[v4(40)]", views: emailView!, passwordView!, loginButton, facebookButton!, signInButton)
       
    }
    @objc func loginButtonClicked() {
        
        
      
        
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [.publicProfile], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPerm,let declinedPerm, let accessToken):
                print("Logged in!")
                self.apikey = accessToken.authenticationToken
            }
        }
        }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
