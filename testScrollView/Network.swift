//
//  Network.swift
//  testScrollView
//
//  Created by Zebre on 16/03/2018.
//  Copyright © 2018 Zebre. All rights reserved.
//

import UIKit
import Foundation
import Alamofire
import ObjectMapper
import AlamofireObjectMapper


class Network {
    static let sharedInstance : Network = {
        let instance = Network()
        return instance
    }()
    
    enum Login {
        case defaultLogin
        case googleLogin
        case facebookLogin
    }
    
    var currentUser: User?
    var listUser : [User]?
    var listTaskPierre : [Task]?
    var listTaskLaurent : [Task]?
    var listTaskAbdelhamid : [Task]?
    var listTaskNicolas : [Task]?
    
    
    
    func initData(){
         let diceRoll = Int(arc4random_uniform(4))
        
        initUser()
        switch diceRoll{
        case 0:
            currentUser = listUser?[0]
        case 1:
            currentUser = listUser?[1]
        case 2:
            currentUser = listUser?[2]
        case 3:
            currentUser = listUser?[3]
        default:
            currentUser = listUser?[0]
        }
       
        initTaskPierre()
        initTaskLaurent()
        initTaskAbdelhamid()
        initTaskNicolas()
        
        print("TaskPierre : \(listTaskPierre?.toJSONString())")
        print("TaskLaurent : \(listTaskLaurent?.toJSONString())")
        print("TaskAbdelhamid : \(listTaskAbdelhamid?.toJSONString())")
        print("TaskNicolas : \(listTaskNicolas?.toJSONString())")
    }
    
    func initTaskNicolas()
    {
        listTaskNicolas = [Task]()
        listTaskNicolas?.append(Task(JSON: [
            "id":10,
            "owner": [
                "id":4,
                "picture":"pictureNicolas"
            ],
            "title": "TacheNicolas 1",
            "level":3,
            "description":"Projet doc",
            "date": "7 Janvier 2018",
            "users":[
                [
                    "id":1,
                    "picture":"picturePierre"
                ],
                [
                    "id":2,
                    "picture":"pictureLaurent"
                ]
            ]
            ])!)
        
        
        listTaskAbdelhamid?.append(Task(JSON: [
            "id":11,
            "owner": [
                "id":4,
                "picture":"pictureNicolas"
            ],
            "title": "Application test",
            "level":2,
            "description":"tester l'application",
            "date": "19 Mars 2018",
            "users":[
                [
                    "id":1,
                    "picture":"picturePierre"
                ],
                [
                    "id":2,
                    "picture":"pictureLaurent"
                ],
                [
                    "id":3,
                    "picture":"pictureAbdelhamid"
                ]
            ]
            ])!)
        
        
        
        
    }
    
    func initTaskAbdelhamid()
    {
        listTaskAbdelhamid = [Task]()
        listTaskAbdelhamid?.append(Task(JSON: [
            "id":7,
            "owner": [
                "id":3,
                "picture":"pictureAbdelhamid"
            ],
            "title": "Tache Abdelhamid 1",
            "level":4,
            "description":"Projet api + client",
            "date": "7 Décembre 2018",
            "users":[
                [
                    "id":1,
                    "picture":"picturePierre"
                ],
                [
                    "id":4,
                    "picture":"pictureNicolas"
                ]
            ]
            ])!)
        
        
        listTaskAbdelhamid?.append(Task(JSON: [
            "id":8,
            "owner": [
                "id":3,
                "picture":"pictureAbdelhamid"
            ],
            "title": "Application ios",
            "level":1,
            "description":"fin des taches",
            "date": "20 Septembre 2018",
            "users":[
                [
                    "id":1,
                    "picture":"picturePierre"
                ],
                [
                    "id":2,
                    "picture":"pictureLaurent"
                ],
                [
                    "id":4,
                    "picture":"pictureNicolas"
                ]
            ]
            ])!)
        
        
        
        
    }
    
    func initTaskLaurent()
    {
        listTaskLaurent = [Task]()
        listTaskLaurent?.append(Task(JSON: [
            "id":4,
            "owner": [
                "id":2,
                "picture":"pictureLaurent"
            ],
            "title": "Tache Laurent 1",
            "level":4,
            "description":"Projet api",
            "date": "10 Décembre 2018",
            "users":[
                [
                    "id":1,
                    "picture":"picturePierre"
                ],
                [
                    "id":3,
                    "picture":"pictureAbdelhamid"
                ]
            ]
            ])!)
        
        
        listTaskLaurent?.append(Task(JSON: [
            "id":5,
            "owner": [
                "id":2,
                "picture":"pictureLaurent"
            ],
            "title": "Application android",
            "level":1,
            "description":"fin d'ecran de connexion",
            "date": "5 Juin 2018",
            "users":[
                [
                    "id":1,
                    "picture":"picturePierre"
                ],
                [
                    "id":3,
                    "picture":"pictureAbdelhamid"
                ],
                [
                    "id":4,
                    "picture":"pictureNicolas"
                ]
            ]
            ])!)
        
        
        
        
    }
    
    func initTaskPierre()
    {
        listTaskPierre = [Task]()
        listTaskPierre?.append(Task(JSON: [
            "id":1,
            "owner": [
                "id":1,
                "picture":"picturePierre"
            ],
            "title": "Tache 1",
            "level":1,
            "description":"Tache important les amis",
            "date": "12 Janvier 2018",
            "users":[
                        [
                            "id":2,
                            "picture":"pictureLaurent"
                        ],
                        [
                            "id":3,
                            "picture":"pictureAbdelhamid"
                        ]
                    ]
            ])!)
        
        
        listTaskPierre?.append(Task(JSON: [
            "id":2,
            "owner": [
                "id":1,
                "picture":"picturePierre"
            ],
            "title": "Tache 2",
            "level":1,
            "description":"Tache très important les amis",
            "date": "5 Aout 2018",
            "users":[
                [
                    "id":2,
                    "picture":"pictureLaurent"
                ],
                [
                    "id":3,
                    "picture":"pictureAbdelhamid"
                ],
                [
                    "id":4,
                    "picture":"pictureNicolas"
                ]
            ]
            ])!)
        
        
        
        
    }
    
    
    func initUser(){
        listUser = [User]()
        
        listUser?.append(User(JSON: [
            "id":1,
            "username":"Pierre",
            "description":"C'est la description de Pierre",
            "email":"pierre@email.fr",
            "picture":""])!)
        listUser?.append(User(JSON: [
            "id":2,
            "username":"Laurent",
            "description":"C'est la description de Laurent",
            "email":"laurent@email.fr",
            "picture":""])!)
        listUser?.append(User(JSON: [
            "id":3,
            "username":"Abdelhamid",
            "description":"C'est la description de Abdelhamid",
            "email":"abdelhamid@email.fr",
            "picture":""])!)
        listUser?.append(User(JSON: [
            "id":4,
            "username":"Nicolas",
            "description":"C'est la description de Nicolas",
            "email":"nicolas@email.fr",
            "picture":""])!)
    }

    
    
    
    var savePara: Parameters?
    var ipServer = "http://symfony.localhost:8080/"
    var header: HTTPHeaders = [
        "apikey": "",
        ]
    var listEventIsLock : Bool = true;
    
    
 
    typealias LoginCompletion  = (_ success: Bool) -> Void
    func logIn(type: Login, token: String, password: String, completion : @escaping LoginCompletion){
       
        
        var request = ""
        var param = [String : String]()
        switch type {
        case .defaultLogin:
            request = "\(ipServer)login"
            param = [
                "email" : token,
                "password" : password
            ]
        case .facebookLogin:
            print("facebook")
            request = "\(ipServer)facebook_login"
    
            param = [
                "token" : token
            ]
        case .googleLogin:
            print("google")
            request = "\(ipServer)google_login"
            param = [
                "token" : token
            ]
        }
        
        print("Param \n------ \(param)\n -------")
        print("Request : \(request)")
        Alamofire.request(request, method: .post , parameters: param)
            .responseJSON { response in
                print("Salut")
                print(response.debugDescription)
                if let status = response.response?.statusCode {
                    print("Status code \(status)")
                    switch(status){
                    case 200:
                        //print("200")
                        let JSON = response.value as! NSDictionary
                       //APIKEY// JSON.object(forKey: "apikey")
                        completion(true)
                    case 404:
                        print("Username and Password incorrect")
                       completion(false)
                    case 403:
                        print("Wrong password")
                         completion(false)
                    default:
                        print("error with response status: \(status)")
                        completion(false)
                    }
                }
                else
                {
                    print("Cannot found status")
                }
        }
    }
    
  //* POST EXAMPLE
    
  /*  typealias createItemLocationCompletion  = (_ success: Bool, _ string: String, _ rent: RentResponse?) -> Void
    func createItemLocation(rent: Rent,completion : @escaping createItemLocationCompletion){
        let param = ["rent": [
            
            "name" : rent.name!,
            "note" : rent.note!,
            "quantity": rent.quantity,
            "price": rent.price!,
            ]
        ]
        Alamofire.request("\(ipServer)api/rents.json", method: .post , parameters: param, encoding: JSONEncoding.default, headers: header)
            .responseObject { (response: DataResponse<RentResponse>) in
                //print("\(response.data)    \(response.debugDescription)")
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200:
                        print("200")
                        if let rentresp = response.result.value
                        {
                            completion(true, "\(rent.name) has been created", rentresp)
                        }
                        completion(false, "Something is missing", nil)
                    case 404:
                        completion(false, "Something is missing", nil)
                    case 403:
                        completion(false, "Something is missing", nil)
                    default:
                        completion(false, "Something is missing", nil)
                        
                    }
                }
                completion(false, "Something is missing", nil)
        }
    }
    */
    
    typealias getUsersCompletion  = (_ success: Bool, _ string: String, _ rent: [User]?) -> Void
    func getUsers(completion : @escaping getUsersCompletion){
        
        var listUsers = [User]()
    Alamofire.request("\(ipServer)api/users", method: .get,headers: header)
                    .responseArray { (response: DataResponse<[User]>) in
                        if let status = response.response?.statusCode {
                            switch(status){
                            case 200:
                                if let res = response.result.value
                                {
                                listUsers = res
                                completion(true, "200", listUsers)
                                }
                            default:
                                print("\(self.ipServer)api/users fail")
                                  completion(false, "\(response.response?.statusCode)", nil)
                            }
                        }
                }
            }
   
    
   /* get method
    
    typealias ItemFriendCompletion      = (_ success: Bool, _ string: [RentResponse]?) -> Void
     func getUserItemLocation(userId: [RegisterResponse],completion : @escaping ItemFriendCompletion){
     
     var listRentOfAnEvent = [RentResponse]()
     var i = 0;
     userId.forEach({ element in
     
     if let idUser = element.user?.id
     {
     
     Alamofire.request("\(ipServer)api/users/\(idUser)/rents", method: .get,headers: header)
     .responseArray { (response: DataResponse<[RentResponse]>) in
     if let status = response.response?.statusCode {
     switch(status){
     case 200:
     if let res = response.result.value
     {
     res.forEach({element in
     print("Appended one element \(element.name) \(element.quantity)")
     listRentOfAnEvent.append(element)
     
     })
     i = i + 1
     
     if (i == (userId.count))
     {
     completion(true, listRentOfAnEvent)
     }
     }
     default:
     print("Get rents of user \(idUser) failed")
     }
     }
     }
     }
     })
     
     
     
     }
    */
 
    
    /* PUT example
    
    func changeProfilePicture(picBase64: String)
    {
        let toto: Parameters = [
            "user" : [
                "picture":picBase64
            ]
        ]
        self.header.updateValue("application/json", forKey: "Content-Type")
        Alamofire.request("\(ipServer)api/user/me", method: .put, parameters: toto, encoding: JSONEncoding.default, headers: header)
            .validate()
            .responseJSON { response in
                if let status = response.response?.statusCode {
                    switch(status){
                    case 200:
                        print("200 CBIV")
                        //                        NotificationCenter.default.post(name: Notification.Name(rawValue: keyEventUpdated), object: self)
                        
                    case 404:
                        print("Username and Password incorrect")
                        print(response)
                        
                    case 403:
                        print("Wrong password")
                    default:
                        print("error with response status: \(status)")
                    }
                }
        }
        
        
    }
    
    */
    
}

