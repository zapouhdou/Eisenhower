//
//  Task.swift
//  testScrollView
//
//  Created by Zebre on 16/03/2018.
//  Copyright © 2018 Zebre. All rights reserved.
//
import ObjectMapper
import AlamofireObjectMapper

class Task : Mappable {
    var id: Int?
    var owner: UserLess? = nil
    var title:String? = ""
    var level: Int?
    var description: String? = ""
    var date: String? = ""
    var users:[UserLess]? = nil
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        owner <- map["owner"]
        title <- map["title"]
        level <- map["level"]
        description <- map["description"]
        date <- map["date"]
        users <- map["users"]
    }
}
