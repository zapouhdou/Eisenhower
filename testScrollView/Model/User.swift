//
//  User.swift
//  testScrollView
//
//  Created by Zebre on 16/03/2018.
//  Copyright © 2018 Zebre. All rights reserved.
//

import ObjectMapper
import AlamofireObjectMapper

class User : Mappable {
    var id: Int?
    var username: String? = ""
    var description: String? = ""
    var email:String? = ""
    var picture:String? = ""
  
    required init?(map: Map){
        
        
    }
    func mapping(map: Map) {
        id <- map["id"]
        username <- map["username"]
        description <- map["description"]
        email <- map["email"]
        picture <- map["picture"]
    }
    
}

class UserLess : Mappable {
    var id: Int?
    var picture:String? = ""
    
    required init?(map: Map){
        
        
    }
    func mapping(map: Map) {
        id <- map["id"]
        picture <- map["picture"]
    }
    
}
