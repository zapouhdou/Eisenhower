//
//  CreateTask.swift
//  testScrollView
//
//  Created by Zebre on 16/03/2018.
//  Copyright © 2018 Zebre. All rights reserved.
//

import ObjectMapper
import AlamofireObjectMapper

class CreateTask : Mappable {
    var title:String? = ""
    var level: Int? = 0
    var description: String? = ""
    var date: String? = ""
    
    required init?(map: Map){
    }
    
    func mapping(map: Map) {
        title <- map["title"]
        level = map["level"].currentValue as! Int
        description <- map["description"]
        date <- map["date"]
    }
}

